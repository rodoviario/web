
<!DOCTYPE html>
<html lang="es">
    <head>
        <meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <title>LUG Tucuman</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
        <link rel="stylesheet" href="styles.css" type="text/css" media="all" />
        <link href="https://fonts.googleapis.com/css?family=Fira+Sans|Roboto+Slab" rel="stylesheet">
    </head>

    <body>
        <div class="container content"> 
            <div class="pagehead jumbotron"> 
                <div class="row">
                    <div class="col-md-3">
                        <img src="http://lugtucuman.usla.org.ar/logo.png">
                    </div>
                    <div class="col-md-9">
                        <h1>Linux User Group Tucumán</h1>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="lateral col-md-3">
                    <div class="linkhead"><h2>Información</h2></div>

                    <ul>

                        <li>
                            <a href="https://listas.usla.org.ar/cgi-bin/mailman/listinfo/tucuman-lst">
                                Lista de correo
                            </a>
                        </li>
                        <li>
                            <a href="http://lugtucuman.usla.org.ar/bulletin/phpBB3/">
                                Foro
                            </a>
                        </li>
                        <li>
                            <a href="https://www.facebook.com/lugtucuman/">
                                Página de Facebook
                            </a>
                        </li>
                        <li>
                            <a href="https://www.youtube.com/channel/UC4io3bgQ3bMPNRqZHFhu9pA">
                                Canal de Youtube
                            </a>
                        </li>
                        <li>
                            <a href="https://flisol.info/FLISOL2018/Argentina/SanMiguel">
                                Wiki FLISOL 2018 Tucumán
                            </a>
                        </li>


                    </ul>

                </div>
                <div class="col-md-6">
                    <p>
                        Bienvenido a la página oficial de Linux User Group Tucumán. Si bien nuestro tema principal es GNU/Linux, le damos una gran importancia  
                    la filosofía del software libre.
                    </p>
                    <p>
                        Por lo general coordinamos reuniones todas las semanas, las cuales son abiertas a todo aquel que tenga interés en aprender, enseñar y/o 
                        participar. 
                    </p>
                </div>
                <div class="col-md-3"></div>
                <div class="meeting col-md-3">
                        <!--
                        Aqui hay que abrir leer y mostarr el archivo meet
                        -->
                        <?php
                        $myfile = fopen("meet", "r") or die("Unable to open file!");

                        while (!feof($myfile)) {
                            echo fgets($myfile);
                        }
                        fclose($myfile);
                        ?>

                    </div>
                <div class="central col-md-6">
                    <div class="apologies">
                    </div>
                    <iframe hidden src="https://calendar.google.com/calendar/b/2/embed?showNav=0&amp;showDate=0&amp;showPrint=0&amp;showTabs=0&amp;showCalendars=0&amp;showTz=0&amp;mode=AGENDA&amp;height=200&amp;wkst=1&amp;hl=es_419&amp;bgcolor=%23FFFFFF&amp;src=2i95l067b3vrjgdj5um50o08q0%40group.calendar.google.com&amp;color=%23711616&amp;ctz=America%2FArgentina%2FBuenos_Aires" style="border-width:0" width="500" height="200" frameborder="0" scrolling="no"></iframe>

                    <div class="news">
                        <div class="linkhead"><h2>Noticias</h2></div>
                        <p>
                            <?php
                            $myfile = fopen("news", "r") or die("Unable to open file!");
                            $i = 0;
                            while (!feof($myfile)) {
                                echo "<b>" . fgets($myfile) . "</b>" . "<br>";
                                echo fgets($myfile) . "<br>";
                                $i++;
                                if ($i == 3) {
                                    break;
                                }
                            }
                            fclose($myfile);
                            ?> 

                        </p>
                        <p>
                            <a href="subscribirse.php?pr=yes">Subscribirse a las Noticias del grupo</a>
                        </p>
                    </div>

                </div>
            </div>
        </div>
    </body>

</html>
