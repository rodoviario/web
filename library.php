<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function prepareStr($string){
    $array = str_split($string);
    $out = '';
    foreach($array as $char){
        if($char != "\n"){
            $out.=$char;
        }  else {
            $out.='<br>';
        }
    }
    $out.="\n";
    return $out;
}

function writeNews($subject,$message){
    if($subject == NULL | $message == NULL){
        return FALSE;
    }  else {
        $myfile = fopen("news", "r") or die("Unable to open file!");
        $i = 0;
        
        while (!feof($myfile)) {
            $a[$i++] = fgets($myfile);
            $a[$i++] = fgets($myfile);
        }
        fclose($myfile);
        $message = prepareStr($message);
        $subject = prepareStr($subject);
        
        if($a == null){
            $wf = fopen("news", "w") or die("Unable to open news!");
            fwrite($wf, $subject);
            fwrite($wf, $message);
            fclose($wf);
        }  else {
            $wf = fopen("news", "w") or die("Unable to open news!");
            fwrite($wf, $subject);
            fwrite($wf, $message);
            foreach ($a as $j => $v) {
                fwrite($wf, $v);
            }
            fclose($wf);
            
        }
        return TRUE;
    }
}

function newsNotYetSend($subject){
    if($subject == NULL){
        return FALSE;
    }
    $ban = FALSE;
        $myfile = fopen("lastnews", "r") or die("Unable to open file!");
        if (!feof($myfile)) {
            $str = fgets($myfile);
            if (count($str) < count($subject) | count($str) > count($subject)) {
                $ban = TRUE;              
            } else {
                
                for ($index = 0; $index < strlen($subject) - 1; $index++) {                    
                    if ($str[$index] != $subject[$index]) {
                        $ban = TRUE;                        
                        break;
                    }
                }
            }
        }
        fclose($myfile);
        return $ban;
}

function saveLastSend($subject){
    $myfile = fopen("lastnews", "w") or die("Unable to open file!");
    fwrite($myfile, $subject);
    fclose($myfile);
}

function getLastNews(){
    $myfile = fopen("news", "r") or die("Unable to open file!");
    if (!feof($myfile)) {
        $out['subject'] = fgets($myfile);
        $out['message'] = fgets($myfile);
        return $out;
    }
    fclose($myfile);
    
}

function sendNews(){
    $a = getLastNews();
    $subject = $a['subject'];
    $message = $a['message'];
    $additional_headers = 'MIME-Version: 1.0' . "\r\n";
    $additional_headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
    $additional_headers .= "From: noreply@lugtucuman.usla.org.ar" . "\r\n";
    if (\newsNotYetSend($subject) === \TRUE) {
        $myfile = fopen("mails", "r") or die("Unable to open file!");
        while (!feof($myfile)) {
            $to = trim(fgets($myfile), "\n");
            $sent = mail($to, $subject, '<!DOCTYPE html>
            <!--
            To change this license header, choose License Headers in Project Properties.
            To change this template file, choose Tools | Templates
            and open the template in the editor.
            -->
            <html>
                <head>
                    <title></title>
                    <meta charset="UTF-8">
                    <meta name="viewport" content="width=device-width, initial-scale=1.0">
                </head>
                <body>
                    <div>' . $message . '<br>'
                    . '<a href="http://lugtucuman.usla.org.ar/unsubscribe.php?email=' . $to . '">No quiero recibir más noticias</a><br><br>'
                    . '<a href="http://lugtucuman.usla.org.ar">Ir a la página del LUG Tucumán</a>'
                . '</body>
            </html>', $additional_headers);
            if ($sent) {
                echo "<b> Email has been successfully sent </b><br><br>";
                echo "<b> Message : </b><br>$message";
                $ban = TRUE;
            } else {
                echo "Email could not be sent, there may be errors in the e-mail address";
            }
        }
        fclose($myfile);
        saveLastSend($subject);
    } else {
        echo 'Noticia ya enviada<br>';
    }
}